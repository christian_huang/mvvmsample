//
//  FormViewController.swift
//  MVVMSample
//
//  Created by Christian Christian on 2020-02-28.
//  Copyright © 2020 Christian Christian. All rights reserved.
//

import UIKit

class FormViewController: UIViewController {

    @IBOutlet private var firstNameTextField: UITextField!
    @IBOutlet private var lastNameTextField: UITextField!
    @IBOutlet private var combineButton: UIButton!
    @IBOutlet private var fullnameLabel: UILabel!

    private let formViewModel: FormViewModel = DefaultFormViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        initViewModel()
    }

    @IBAction private func firstNameTextFieldChanged(_ sender: UITextField) {
        formViewModel.updateFirstName(firstNameTextField.text)
    }
    @IBAction private func lastNameTextFieldChanged(_ sender: UITextField) {
        formViewModel.updateLastName(lastNameTextField.text)
    }
    @IBAction private func combineButtonDidTapped(_ sender: UIButton) {
        formViewModel.combineButtonDidTapped()
    }
}

// MARK: - ViewModel related
extension FormViewController {
    private func initViewModel() {
        formViewModel.updateFullNameHandler = { [weak self] fullname in
            DispatchQueue.main.async {
                self?.updateFullName(fullname)
            }
        }
    }
}

// MARK: - private func
extension FormViewController {
    private func initView() {
        combineButton.layer.cornerRadius = 10
    }

    private func updateFullName(_ fullName: String) {
        fullnameLabel.text = fullName
    }
}

