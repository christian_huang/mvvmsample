//
//  FormViewModel.swift
//  MVVMSample
//
//  Created by Christian Christian on 2020-02-28.
//  Copyright © 2020 Christian Christian. All rights reserved.
//

import Foundation

protocol FormViewModel: class {
    var updateFullNameHandler: ((String) -> Void)? { get set }
    func updateFirstName(_ newName: String?)
    func updateLastName(_ newName: String?)
    func combineButtonDidTapped()
}

class DefaultFormViewModel: FormViewModel {
    private var firstName: String?
    private var lastName: String?
    private(set) var fullName: String = "" {
        didSet {
            updateFullNameHandler?(fullName)
        }
    }
    var updateFullNameHandler: ((String) -> Void)?

    func updateFirstName(_ newName: String?) {
        firstName = newName
    }

    func updateLastName(_ newName: String?) {
        lastName = newName
    }

    func combineButtonDidTapped() {
        var fullName = ""
        if let firstName = firstName, !firstName.isEmpty {
            fullName = "\(firstName) "
        }
        if let lastName = lastName {
            fullName = "\(fullName)\(lastName)"
        }
        if fullName.isEmpty {
            fullName = "Please fill the First/Last Name field(s)!"
        }
        self.fullName = fullName
    }
}
